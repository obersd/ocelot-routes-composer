﻿Para crear el contexto de la base de datos, se debe descargar por comandos:

dotnet tool install --global dotnet-ef

Posteriormente se instala desde comandos, en la carpeta donde reside el proyecto:

dotnet add package Microsoft.EntityFrameworkCore.Tools
dotnet add package Microsoft.EntityFrameworkCore.Design
dotnet add package Microsoft.EntityFrameworkCore.Sqlite

Y finalmente la ubicación de la base de datos así:

dotnet ef dbcontext scaffold "Data Source=Database\ocelot.db" "Microsoft.EntityFrameworkCore.Sqlite" -f  -c OcelotContext -o Contexts -v --no-build