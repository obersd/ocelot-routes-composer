﻿CREATE TABLE environments
(
	environmentid integer not null constraint environments_pk primary key autoincrement,
	name text not null
);

create table groups(
	groupid integer not null constraint groups_pk primary key autoincrement,
	name text not null
);

CREATE TABLE "servers"
(
	serverid integer not null constraint servers_pk primary key autoincrement,
	environmentid integer not null,
	groupid integer,
	isgateway int not null,
	appname text not null,
	scheme text not null,
	port int,
	host text not null,
	path text not null,
	swagger text not null,
	foreign key(environmentid) references environments(environmentid),
	foreign key(groupid) references groups(groupid)
);

create table apis
(
	apiid integer not null constraint apis_pk primary key autoincrement,
	groupid integer not null,
	controllerpath text not null,
	functionalitypath text not null,
	httpmethod text not null,
	issecure integer not null,
	securekey text not null,
	foreign key(groupid) references groups(groupid)
);

insert into environments (name) values ('LOCAL');
insert into environments (name) values ('DEV');
insert into environments (name) values ('QA');
insert into environments (name) values ('RTP');

insert into groups (name) values ('GATEWAY');
insert into groups (name) values ('AUTOS');
insert into groups (name) values ('BACKOFFICE');
insert into groups (name) values ('MEMBERSHIP');
insert into groups (name) values ('HOMOLOGATION');
