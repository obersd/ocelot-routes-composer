﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExceptionManager.cs" company="A2net Studios">
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Alex Vargas.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace A2Net.OcelotComposer.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Threading;

    using A2Net.OcelotComposer.Observer;

    /// <summary>
    /// The exception manager.
    /// </summary>
    public class ExceptionManager : IDisposable
    {
        /// <summary>
        /// The instance.
        /// </summary>
        private static readonly Lazy<ExceptionManager> Instance;

        /// <summary>
        /// Initializes static members of the <see cref="ExceptionManager"/> class.
        /// </summary>
        static ExceptionManager()
        {
            Instance = new Lazy<ExceptionManager>(() => new ExceptionManager(), LazyThreadSafetyMode.ExecutionAndPublication);
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="ExceptionManager"/> class from being created.
        /// </summary>
        private ExceptionManager()
        {
            this.Exceptions = new List<Exception>();
        }

        /// <summary>
        /// The current.
        /// </summary>
        public static ExceptionManager Current => Instance.Value;

        /// <summary>
        /// The exceptions.
        /// </summary>
        public IList<Exception> Exceptions { get; }

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>
        /// The <see cref="ObservableExceptions"/>.
        /// </returns>
        public ObservableExceptions Create() => new ObservableExceptions(this.Exceptions);

        /// <inheritdoc />
        public void Dispose()
        {
            this.Exceptions.Clear();
        }
    }
}