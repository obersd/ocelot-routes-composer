﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StringPaths.cs" company="A2net Studios">
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Alex Vargas.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace A2Net.OcelotComposer.Helpers
{
    using System;
    using System.Linq;

    using A2Net.OcelotComposer.Observer;

    /// <summary>
    /// The string paths.
    /// </summary>
    public static class StringPaths
    {
        /// <summary>
        /// The append.
        /// </summary>
        /// <param name="uri">
        /// The uri.
        /// </param>
        /// <param name="paths">
        /// The paths.
        /// </param>
        /// <returns>
        /// The <see cref="Uri"/>.
        /// </returns>
        public static Uri Append(this Uri uri, params string[] paths)
        {
            try
            {
                var end = paths.Aggregate(uri.AbsoluteUri, (current, path) => $"{current.TrimEnd('/')}/{path.TrimStart('/')}");
                if (!end.EndsWith("/"))
                {
                    end = $"{end}/";
                }

                return new Uri(end);
            }
            catch (Exception ex)
            {
                MessageService.Current.NotifyException(ex, "Error al agregar segmentos al Uri.", new { Client = "StringPaths.Append", Context = new { uri, paths } });
                return new Uri("127.0.0.1");
            }
        }

        /// <summary>
        /// The build.
        /// </summary>
        /// <param name="scheme">
        /// The scheme.
        /// </param>
        /// <param name="host">
        /// The host.
        /// </param>
        /// <param name="port">
        /// The port.
        /// </param>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <returns>
        /// The <see cref="Uri"/>.
        /// </returns>
        public static Uri Build(string scheme, string host, decimal port, string path)
        {
            try
            {
                var x = new UriBuilder { Scheme = scheme, Host = host, Path = path };
                if (port > 0)
                {
                    x.Port = (int)port;
                }

                return x.Uri;
            }
            catch (Exception ex)
            {
                MessageService.Current.NotifyException(ex, "Error al crear un Uri.", new { Client = "StringPaths.Build", Context = new { scheme, host, port, path } });
                return new Uri("127.0.0.1");
            }
        }
    }
}