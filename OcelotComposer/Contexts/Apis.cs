﻿namespace A2Net.OcelotComposer.Contexts
{
    public partial class Apis
    {
        public long Apiid { get; set; }
        public long Groupid { get; set; }
        public string Controllerpath { get; set; }
        public string Functionalitypath { get; set; }
        public string Httpmethod { get; set; }
        public long Issecure { get; set; }
        public string Securekey { get; set; }

        public virtual Groups Group { get; set; }
    }
}
