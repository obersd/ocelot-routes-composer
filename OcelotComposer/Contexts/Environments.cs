﻿namespace A2Net.OcelotComposer.Contexts
{
    using System.Collections.Generic;

    public partial class Environments
    {
        public Environments()
        {
            this.Servers = new HashSet<Servers>();
        }

        public long Environmentid { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Servers> Servers { get; set; }
    }
}
