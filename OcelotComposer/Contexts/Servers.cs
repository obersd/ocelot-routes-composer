﻿namespace A2Net.OcelotComposer.Contexts
{
    public partial class Servers
    {
        public long Serverid { get; set; }
        public long Environmentid { get; set; }
        public long? Groupid { get; set; }
        public long Isgateway { get; set; }
        public string Appname { get; set; }
        public string Scheme { get; set; }
        public long Port { get; set; }
        public string Host { get; set; }
        public string Path { get; set; }
        public string Swagger { get; set; }

        public virtual Environments Environment { get; set; }
        public virtual Groups Group { get; set; }
    }
}
