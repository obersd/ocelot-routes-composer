﻿namespace A2Net.OcelotComposer.Contexts
{
    using Microsoft.EntityFrameworkCore;

    public partial class OcelotContext : DbContext
    {
        public static OcelotContext GetContext()
        {
            return new OcelotContext();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlite("Data Source=C:\\src\\ocelot-routes-composer\\OcelotComposer\\Database\\ocelot.db");
            }
        }
    }
}
