﻿namespace A2Net.OcelotComposer.Contexts
{
    using System.Collections.Generic;

    public partial class Groups
    {
        public Groups()
        {
            this.Apis = new HashSet<Apis>();
            this.Servers = new HashSet<Servers>();
        }

        public long Groupid { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Apis> Apis { get; set; }
        public virtual ICollection<Servers> Servers { get; set; }
    }
}
