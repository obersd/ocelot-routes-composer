﻿namespace A2Net.OcelotComposer.Contexts
{
    using Microsoft.EntityFrameworkCore;

    public partial class OcelotContext : DbContext
    {
        public OcelotContext()
        {
        }

        public OcelotContext(DbContextOptions<OcelotContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Apis> Apis { get; set; }
        public virtual DbSet<Environments> Environments { get; set; }
        public virtual DbSet<Groups> Groups { get; set; }
        public virtual DbSet<Servers> Servers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Apis>(entity =>
            {
                entity.HasKey(e => e.Apiid);

                entity.ToTable("apis");

                entity.Property(e => e.Apiid)
                    .HasColumnName("apiid")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Controllerpath)
                    .IsRequired()
                    .HasColumnName("controllerpath");

                entity.Property(e => e.Functionalitypath)
                    .IsRequired()
                    .HasColumnName("functionalitypath");

                entity.Property(e => e.Groupid).HasColumnName("groupid");

                entity.Property(e => e.Httpmethod)
                    .IsRequired()
                    .HasColumnName("httpmethod");

                entity.Property(e => e.Issecure).HasColumnName("issecure");

                entity.Property(e => e.Securekey)
                    .IsRequired()
                    .HasColumnName("securekey");

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.Apis)
                    .HasForeignKey(d => d.Groupid)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Environments>(entity =>
            {
                entity.HasKey(e => e.Environmentid);

                entity.ToTable("environments");

                entity.Property(e => e.Environmentid)
                    .HasColumnName("environmentid")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name");
            });

            modelBuilder.Entity<Groups>(entity =>
            {
                entity.HasKey(e => e.Groupid);

                entity.ToTable("groups");

                entity.Property(e => e.Groupid)
                    .HasColumnName("groupid")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name");
            });

            modelBuilder.Entity<Servers>(entity =>
            {
                entity.HasKey(e => e.Serverid);

                entity.ToTable("servers");

                entity.Property(e => e.Serverid)
                    .HasColumnName("serverid")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Appname)
                    .IsRequired()
                    .HasColumnName("appname");

                entity.Property(e => e.Environmentid).HasColumnName("environmentid");

                entity.Property(e => e.Groupid).HasColumnName("groupid");

                entity.Property(e => e.Host)
                    .IsRequired()
                    .HasColumnName("host");

                entity.Property(e => e.Isgateway)
                    .HasColumnName("isgateway")
                    .HasColumnType("int");

                entity.Property(e => e.Path)
                    .IsRequired()
                    .HasColumnName("path");

                entity.Property(e => e.Port)
                    .HasColumnName("port")
                    .HasColumnType("int");

                entity.Property(e => e.Scheme)
                    .IsRequired()
                    .HasColumnName("scheme");

                entity.Property(e => e.Swagger)
                    .IsRequired()
                    .HasColumnName("swagger");

                entity.HasOne(d => d.Environment)
                    .WithMany(p => p.Servers)
                    .HasForeignKey(d => d.Environmentid)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.Servers)
                    .HasForeignKey(d => d.Groupid);
            });

            this.OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
