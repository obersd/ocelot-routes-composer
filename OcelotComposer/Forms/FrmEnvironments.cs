﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FrmEnvironments.cs" company="A2net Studios">
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Alex Vargas.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace A2Net.OcelotComposer.Forms
{
    using System.Linq;
    using System.Windows.Forms;

    using A2Net.OcelotComposer.Contexts;
    using A2Net.OcelotComposer.Models;
    using A2Net.OcelotComposer.Observer;

    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The frm environments.
    /// </summary>
    public partial class FrmEnvironments : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FrmEnvironments"/> class.
        /// </summary>
        public FrmEnvironments()
        {
            this.InitializeComponent();
            this.LoadEnvironments();
            this.dgEnvironments.DataError += (sender, args) => MessageService.Current.NotifyException(
                args.Exception,
                "Error en DataGrid de Environments",
                new { Client = "Environments", Context = args });
            this.btnNew.Click += (sender, args) =>
                {
                    var result = FrmGroupEnvironment.ShowEnvironment(this);
                    if (result)
                    {
                        this.LoadEnvironments();
                    }
                };
        }

        /// <summary>
        /// The show.
        /// </summary>
        /// <param name="wnd">
        /// The wnd.
        /// </param>
        /// <returns>
        /// The <see cref="Form"/>.
        /// </returns>
        public static Form Show(Form wnd)
        {
            var frm = new FrmEnvironments { MdiParent = wnd };
            frm.Show();
            return frm;
        }

        /// <summary>
        /// The dg environments_ cell content click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void DgEnvironmentsCellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                var id = (long)senderGrid["Environmentid", e.RowIndex].Value;
                switch (e.ColumnIndex)
                {
                    case 1:
                        var result = FrmGroupEnvironment.ShowEnvironment(this, id);
                        if (result)
                        {
                            this.LoadEnvironments();
                        }

                        break;

                    case 2:
                        using (var ctx = OcelotContext.GetContext())
                        {
                            var entity = ctx.Environments.Include(x => x.Servers).FirstOrDefault(x => x.Environmentid == id);
                            if (entity != null && !entity.Servers.Any())
                            {
                                ctx.Environments.Remove(entity);
                                ctx.SaveChanges();
                            }
                        }

                        this.LoadEnvironments();
                        break;
                }
            }
        }

        /// <summary>
        /// The load environments.
        /// </summary>
        private void LoadEnvironments()
        {
            this.dgEnvironments.AutoGenerateColumns = false;
            this.dgEnvironments.DataSource = null;
            this.dgEnvironments.Columns.Clear();
            this.dgEnvironments.Columns.AddRange(
                new DataGridViewTextBoxColumn { HeaderText = "#", DataPropertyName = nameof(Environment.Index) },
                new DataGridViewButtonColumn { Text = "Editar", UseColumnTextForButtonValue = true },
                new DataGridViewButtonColumn { Text = "Eliminar", UseColumnTextForButtonValue = true },
                new DataGridViewTextBoxColumn { Name = "Environmentid", DataPropertyName = "Environmentid", HeaderText = "Environmentid", Visible = false },
                new DataGridViewTextBoxColumn { Name = "Name", DataPropertyName = "Name", HeaderText = "Name" });
            using (var ctx = OcelotContext.GetContext())
            {
                var index = 1;
                this.dgEnvironments.DataSource = ctx.Environments.AsEnumerable().Select(x => new Environment(x, index++)).ToList();
            }
        }
    }
}