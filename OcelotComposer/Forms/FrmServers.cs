﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FrmServers.cs" company="A2net Studios">
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Alex Vargas.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace A2Net.OcelotComposer.Forms
{
    using System.Linq;
    using System.Windows.Forms;

    using A2Net.OcelotComposer.Contexts;
    using A2Net.OcelotComposer.Models;
    using A2Net.OcelotComposer.Observer;

    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The frm servers.
    /// </summary>
    public partial class FrmServers : Form
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="FrmServers"/> class from being created.
        /// </summary>
        private FrmServers()
        {
            this.InitializeComponent();
            this.dgServers.DataError += (sender, args) => MessageService.Current.NotifyException(
                args.Exception,
                "Error en DataGrid de Servers",
                new { Client = "Servers", Context = args });
            this.btnAdd.Click += (sender, args) =>
                {
                    var result = FrmServerInfo.ShowWindow(this);
                    if (result)
                    {
                        this.LoadServers();
                    }
                };
            this.LoadServers();
        }

        /// <summary>
        /// The show.
        /// </summary>
        /// <param name="wnd">
        /// The wnd.
        /// </param>
        /// <returns>
        /// The <see cref="Form"/>.
        /// </returns>
        public static Form Show(Form wnd)
        {
            var frm = new FrmServers { MdiParent = wnd };
            frm.Show();
            return frm;
        }

        /// <summary>
        /// The dg servers_ cell content click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void DgServersCellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                var id = (long)senderGrid["ServerId", e.RowIndex].Value;
                switch (e.ColumnIndex)
                {
                    case 1:
                        var result = FrmServerInfo.ShowWindow(this, id);
                        if (result)
                        {
                            this.LoadServers();
                        }

                        break;
                    case 2:
                        var resultEx = MessageBox.Show("¿Eliminar Server?", "Ocelot Composer", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (resultEx != DialogResult.Yes)
                        {
                            return;
                        }

                        using (var ctx = OcelotContext.GetContext())
                        {
                            var entity = ctx.Servers.FirstOrDefault(x => x.Serverid == id);
                            if (entity != null)
                            {
                                ctx.Servers.Remove(entity);
                                ctx.SaveChanges();
                            }
                        }

                        this.LoadServers();
                        break;
                }
            }
        }

        /// <summary>
        /// The load servers.
        /// </summary>
        private void LoadServers()
        {
            this.dgServers.AutoGenerateColumns = false;
            this.dgServers.DataSource = null;
            this.dgServers.Columns.Clear();
            this.dgServers.Columns.AddRange(
                new DataGridViewTextBoxColumn { HeaderText = "#", DataPropertyName = nameof(Server.Index) },
                new DataGridViewButtonColumn { Text = "Editar", UseColumnTextForButtonValue = true },
                new DataGridViewButtonColumn { Text = "Eliminar", UseColumnTextForButtonValue = true },
                new DataGridViewTextBoxColumn { Name = "ServerId", DataPropertyName = "ServerId", HeaderText = "ServerId", Visible = false },
                new DataGridViewTextBoxColumn { Name = "GroupName", DataPropertyName = "GroupName", HeaderText = "Group" },
                new DataGridViewTextBoxColumn { Name = "AppName", DataPropertyName = "AppName", HeaderText = "AppName" },
                new DataGridViewTextBoxColumn { Name = "Environment", DataPropertyName = "EnvironmentName", HeaderText = "Environment" },
                new DataGridViewTextBoxColumn { Name = "Scheme", DataPropertyName = "Scheme", HeaderText = "Scheme" },
                new DataGridViewTextBoxColumn { Name = "Host", DataPropertyName = "Host", HeaderText = "Host" },
                new DataGridViewTextBoxColumn { Name = "Port", DataPropertyName = "Port", HeaderText = "Port" },
                new DataGridViewTextBoxColumn { Name = "Path", DataPropertyName = "Path", HeaderText = "Path" },
                new DataGridViewCheckBoxColumn { Name = "IsGateway", DataPropertyName = "IsGateway", HeaderText = "IsGateway" },
                new DataGridViewTextBoxColumn { Name = "Swagger", DataPropertyName = "Swagger", HeaderText = "Swagger" },
                new DataGridViewTextBoxColumn { Name = "Complete", DataPropertyName = "Complete", HeaderText = "server URL" },
                new DataGridViewLinkColumn { Name = "SwaggerComplete", DataPropertyName = "SwaggerComplete", HeaderText = "Swagger URL" });
            using (var ctx = OcelotContext.GetContext())
            {
                var index = 1;
                this.dgServers.DataSource = ctx.Servers.Include(x => x.Environment).Include(x => x.Group).AsEnumerable().Select(x => new Server(x, index++)).ToList();
            }
        }
    }
}