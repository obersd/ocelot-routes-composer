﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FrmPrincipal.cs" company="A2net Studios">
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Alex Vargas.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace A2Net.OcelotComposer.Forms
{
    using System.Windows.Forms;

    using A2Net.OcelotComposer.Helpers;

    /// <summary>
    /// The frm principal.
    /// </summary>
    public partial class FrmPrincipal : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FrmPrincipal"/> class.
        /// </summary>
        public FrmPrincipal()
        {
            this.InitializeComponent();
            this.InitEvents();
            WindowManager.Current.PrincipalForm = this;
            this.Closed += (sender, args) =>
                {
                    WindowManager.Current.Dispose();
                    ExceptionManager.Current.Dispose();
                };
        }

        /// <summary>
        /// The init events.
        /// </summary>
        private void InitEvents()
        {
            this.mniEnvironments.Click += (sender, args) => WindowManager.Current.OpenChild(WindowManager.Window.Environments);
            this.mniGroups.Click += (sender, args) => WindowManager.Current.OpenChild(WindowManager.Window.Groups);
            this.mniServers.Click += (sender, args) => WindowManager.Current.OpenChild(WindowManager.Window.Servers);
            this.mniApis.Click += (sender, args) => WindowManager.Current.OpenChild(WindowManager.Window.Apis);
            this.mniGenerator.Click += (sender, args) => WindowManager.Current.OpenChild(WindowManager.Window.Composer);
            this.mniViewer.Click += (sender, args) => WindowManager.Current.OpenChild(WindowManager.Window.Logger);

            this.mniCascade.Click += (sender, args) => this.LayoutMdi(MdiLayout.Cascade);
            this.mniArrange.Click += (sender, args) => this.LayoutMdi(MdiLayout.ArrangeIcons);
            this.mniH.Click += (sender, args) => this.LayoutMdi(MdiLayout.TileHorizontal);
            this.mniV.Click += (sender, args) => this.LayoutMdi(MdiLayout.TileVertical);
            this.mniQuit.Click += (sender, args) => Application.Exit();
        }
    }
}