﻿namespace A2Net.OcelotComposer.Forms
{
    partial class FrmServerInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtHost = new System.Windows.Forms.TextBox();
            this.lstSchemes = new System.Windows.Forms.ComboBox();
            this.nudPort = new System.Windows.Forms.NumericUpDown();
            this.txtPart = new System.Windows.Forms.TextBox();
            this.lstEnvs = new System.Windows.Forms.ComboBox();
            this.txtSwagger = new System.Windows.Forms.TextBox();
            this.chkGateway = new System.Windows.Forms.CheckBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtComplete = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.lstGroup = new System.Windows.Forms.ComboBox();
            this.btnView = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nudPort)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "Scheme:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(137, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 15);
            this.label3.TabIndex = 0;
            this.label3.Text = "Host:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(377, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 15);
            this.label4.TabIndex = 0;
            this.label4.Text = "Port:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 15);
            this.label5.TabIndex = 0;
            this.label5.Text = "Path:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(224, 110);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 15);
            this.label8.TabIndex = 0;
            this.label8.Text = "Swagger:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 114);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 15);
            this.label9.TabIndex = 0;
            this.label9.Text = "Entorno:";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(72, 20);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(192, 23);
            this.txtName.TabIndex = 1;
            // 
            // txtHost
            // 
            this.txtHost.Location = new System.Drawing.Point(178, 49);
            this.txtHost.Name = "txtHost";
            this.txtHost.Size = new System.Drawing.Size(193, 23);
            this.txtHost.TabIndex = 4;
            // 
            // lstSchemes
            // 
            this.lstSchemes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lstSchemes.FormattingEnabled = true;
            this.lstSchemes.Location = new System.Drawing.Point(72, 49);
            this.lstSchemes.Name = "lstSchemes";
            this.lstSchemes.Size = new System.Drawing.Size(59, 23);
            this.lstSchemes.TabIndex = 3;
            // 
            // nudPort
            // 
            this.nudPort.Location = new System.Drawing.Point(415, 49);
            this.nudPort.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nudPort.Name = "nudPort";
            this.nudPort.Size = new System.Drawing.Size(120, 23);
            this.nudPort.TabIndex = 5;
            this.nudPort.Enter += new System.EventHandler(this.nudPort_Enter);
            // 
            // txtPart
            // 
            this.txtPart.Location = new System.Drawing.Point(72, 78);
            this.txtPart.Name = "txtPart";
            this.txtPart.Size = new System.Drawing.Size(463, 23);
            this.txtPart.TabIndex = 6;
            // 
            // lstEnvs
            // 
            this.lstEnvs.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lstEnvs.FormattingEnabled = true;
            this.lstEnvs.Location = new System.Drawing.Point(72, 107);
            this.lstEnvs.Name = "lstEnvs";
            this.lstEnvs.Size = new System.Drawing.Size(146, 23);
            this.lstEnvs.TabIndex = 7;
            // 
            // txtSwagger
            // 
            this.txtSwagger.Location = new System.Drawing.Point(285, 107);
            this.txtSwagger.Name = "txtSwagger";
            this.txtSwagger.Size = new System.Drawing.Size(250, 23);
            this.txtSwagger.TabIndex = 8;
            // 
            // chkGateway
            // 
            this.chkGateway.AutoSize = true;
            this.chkGateway.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkGateway.Location = new System.Drawing.Point(12, 140);
            this.chkGateway.Name = "chkGateway";
            this.chkGateway.Size = new System.Drawing.Size(84, 19);
            this.chkGateway.TabIndex = 9;
            this.chkGateway.Text = "Es gateway";
            this.chkGateway.UseVisualStyleBackColor = true;
            this.chkGateway.CheckedChanged += new System.EventHandler(this.ChkGatewayCheckedChanged);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(467, 253);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 25);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Guardar";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(386, 253);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 25);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.Text = "Cancelar";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // txtComplete
            // 
            this.txtComplete.Location = new System.Drawing.Point(14, 180);
            this.txtComplete.Multiline = true;
            this.txtComplete.Name = "txtComplete";
            this.txtComplete.ReadOnly = true;
            this.txtComplete.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtComplete.Size = new System.Drawing.Size(521, 52);
            this.txtComplete.TabIndex = 100;
            this.txtComplete.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(270, 23);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 15);
            this.label7.TabIndex = 13;
            this.label7.Text = "Group:";
            // 
            // lstGroup
            // 
            this.lstGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lstGroup.FormattingEnabled = true;
            this.lstGroup.Location = new System.Drawing.Point(319, 20);
            this.lstGroup.Name = "lstGroup";
            this.lstGroup.Size = new System.Drawing.Size(216, 23);
            this.lstGroup.TabIndex = 2;
            // 
            // btnView
            // 
            this.btnView.Location = new System.Drawing.Point(305, 253);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(75, 25);
            this.btnView.TabIndex = 10;
            this.btnView.Text = "Ver rutas";
            this.btnView.UseVisualStyleBackColor = true;
            this.btnView.Click += new System.EventHandler(this.BtnViewClick);
            // 
            // FrmServerInfo
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(554, 290);
            this.Controls.Add(this.btnView);
            this.Controls.Add(this.lstGroup);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtComplete);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.chkGateway);
            this.Controls.Add(this.txtSwagger);
            this.Controls.Add(this.lstEnvs);
            this.Controls.Add(this.txtPart);
            this.Controls.Add(this.nudPort);
            this.Controls.Add(this.lstSchemes);
            this.Controls.Add(this.txtHost);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmServerInfo";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Server info";
            ((System.ComponentModel.ISupportInitialize)(this.nudPort)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtHost;
        private System.Windows.Forms.ComboBox lstSchemes;
        private System.Windows.Forms.NumericUpDown nudPort;
        private System.Windows.Forms.TextBox txtPart;
        private System.Windows.Forms.ComboBox lstEnvs;
        private System.Windows.Forms.TextBox txtSwagger;
        private System.Windows.Forms.CheckBox chkGateway;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox txtComplete;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox lstGroup;
        private System.Windows.Forms.Button btnView;
    }
}