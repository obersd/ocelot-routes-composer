﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FrmApi.cs" company="A2net Studios">
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Alex Vargas.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace A2Net.OcelotComposer.Forms
{
    using System;
    using System.Linq;
    using System.Windows.Forms;

    using A2Net.OcelotComposer.Contexts;

    /// <summary>
    /// The frm api.
    /// </summary>
    public partial class FrmApi : Form
    {
        /// <summary>
        /// The id.
        /// </summary>
        private readonly long id;

        /// <summary>
        /// Prevents a default instance of the <see cref="FrmApi"/> class from being created.
        /// </summary>
        private FrmApi()
        {
            this.InitializeComponent();
            this.btnSave.Click += this.AddEntity;
            this.Load += (sender, args) => this.Initialize();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FrmApi"/> class.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        private FrmApi(long id)
        {
            this.id = id;
            this.InitializeComponent();
            this.btnSave.Click += this.UpdateEntity;
            this.Load += (sender, args) =>
                {
                    this.Initialize();
                    using (var ctx = OcelotContext.GetContext())
                    {
                        var api = ctx.Apis.FirstOrDefault(x => x.Apiid == this.id);
                        if (api != null)
                        {
                            this.txtController.Text = api.Controllerpath;
                            this.txtFunction.Text = api.Functionalitypath;
                            this.txtSecureKey.Text = api.Securekey;
                            this.chkSecure.Checked = api.Issecure == 1;
                            this.lstGroup.SelectedValue = api.Groupid;
                            this.lstMethod.SelectedItem = api.Httpmethod.ToUpperInvariant();
                        }
                    }
                };
        }

        /// <summary>
        /// The show.
        /// </summary>
        /// <param name="wnd">
        /// The wnd.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool ShowWindow(IWin32Window wnd)
        {
            var frm = new FrmApi();
            var result = frm.ShowDialog(wnd);
            return result == DialogResult.OK;
        }

        /// <summary>
        /// The show.
        /// </summary>
        /// <param name="wnd">
        /// The wnd.
        /// </param>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool ShowWindow(IWin32Window wnd, long id)
        {
            var frm = new FrmApi(id);
            var result = frm.ShowDialog(wnd);
            return result == DialogResult.OK;
        }

        /// <summary>
        /// The add entity.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void AddEntity(object sender, EventArgs e)
        {
            var api = new Apis
            {
                Controllerpath = this.TransformControllerPath(),
                Functionalitypath = this.TransformFunctionality(),
                Httpmethod = this.lstMethod.SelectedItem?.ToString()?.ToLowerInvariant() ?? string.Empty,
                Issecure = this.chkSecure.Checked ? 1 : 0,
                Securekey = this.chkSecure.Checked ? this.txtSecureKey.Text : string.Empty,
                Groupid = (long)this.lstGroup.SelectedValue
            };

            using (var context = OcelotContext.GetContext())
            {
                context.Apis.Add(api);
                context.SaveChanges();
            }

            this.DialogResult = DialogResult.OK;
        }

        /// <summary>
        /// The chk secure_ checked changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void ChkSecureCheckedChanged(object sender, EventArgs e)
        {
            if (this.chkSecure.CheckState == CheckState.Unchecked)
            {
                this.txtSecureKey.Clear();
                this.txtSecureKey.ReadOnly = true;
            }
            else
            {
                this.txtSecureKey.ReadOnly = false;
            }
        }

        /// <summary>
        /// The initialize.
        /// </summary>
        private void Initialize()
        {
            this.btnCancel.DialogResult = DialogResult.Cancel;
            var methods = new[] { "GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS", "HEAD", "CONNECT", "TRACE" };
            this.lstMethod.DataSource = methods;
            using (var ctx = OcelotContext.GetContext())
            {
                this.lstGroup.DisplayMember = nameof(Groups.Name);
                this.lstGroup.ValueMember = nameof(Groups.Groupid);
                this.lstGroup.DataSource = ctx.Groups.OrderBy(x => x.Name).ToList();
            }

            if (this.lstGroup.Items.Count != 0)
            {
                return;
            }

            MessageBox.Show("No existen grupos para agregar una API.", "Ocelot Composer", MessageBoxButtons.OK, MessageBoxIcon.Error);
            this.DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// The transform controller path.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string TransformControllerPath()
        {
            var controllerPath = this.txtController.Text;
            if (controllerPath.StartsWith("/"))
            {
                controllerPath = controllerPath.TrimStart('/');
            }

            if (!controllerPath.EndsWith("/"))
            {
                controllerPath = $"{controllerPath}/";
            }

            return controllerPath;
        }

        /// <summary>
        /// The transform functionality.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string TransformFunctionality()
        {
            var func = this.txtFunction.Text;
            return func.Trim('/');
        }

        /// <summary>
        /// The update entity.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void UpdateEntity(object sender, EventArgs e)
        {
            using (var ctx = OcelotContext.GetContext())
            {
                var api = ctx.Apis.FirstOrDefault(x => x.Apiid == this.id);
                if (api != null)
                {
                    api.Controllerpath = this.TransformControllerPath();
                    api.Functionalitypath = this.TransformFunctionality();
                    api.Httpmethod = this.lstMethod.SelectedItem?.ToString()?.ToLowerInvariant() ?? string.Empty;
                    api.Issecure = this.chkSecure.Checked ? 1 : 0;
                    api.Securekey = this.chkSecure.Checked ? this.txtSecureKey.Text : string.Empty;
                    api.Groupid = (long)this.lstGroup.SelectedValue;
                    ctx.SaveChanges();
                }
            }

            this.DialogResult = DialogResult.OK;
        }
    }
}