﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FrmLogger.cs" company="A2net Studios">
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Alex Vargas.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace A2Net.OcelotComposer.Forms
{
    using System;
    using System.Linq;
    using System.Windows.Forms;

    using A2Net.OcelotComposer.Helpers;
    using A2Net.OcelotComposer.Observer;

    using Newtonsoft.Json;

    /// <summary>
    /// The frm logger.
    /// </summary>
    public partial class FrmLogger : Form, IObserver
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FrmLogger"/> class.
        /// </summary>
        public FrmLogger()
        {
            this.InitializeComponent();
            this.Unsubscriber = MessageService.Current.Subscribe(this);
            this.Closed += (sender, args) => this.OnCompleted();
        }

        /// <inheritdoc />
        public IDisposable Unsubscriber { get; set; }

        /// <summary>
        /// The show window.
        /// </summary>
        /// <param name="form">
        /// The form.
        /// </param>
        /// <returns>
        /// The <see cref="Form"/>.
        /// </returns>
        public static Form ShowWindow(Form form)
        {
            var frm = new FrmLogger { MdiParent = form };
            frm.Show();
            return frm;
        }

        /// <inheritdoc />
        public void OnCompleted()
        {
            this.Unsubscriber?.Dispose();
        }

        /// <inheritdoc />
        public void OnError(Exception error)
        {
            if (error is ObservableExceptions ex)
            {
                if (!ex.Exceptions.Any())
                {
                    return;
                }

                this.txtLog.Clear();
                this.txtLog.AppendText($"Message: {error.Message}");
                this.txtLog.AppendText(Environment.NewLine);

                foreach (var messageItem in ex.Exceptions)
                {
                    this.txtLog.AppendText($"Exception: {messageItem.InnerException?.ToString() ?? string.Empty}");
                    this.txtLog.AppendText(Environment.NewLine);
                    if (!(messageItem is ObservableException observableException))
                    {
                        continue;
                    }

                    this.txtLog.AppendText($"ExtraData: {JsonConvert.SerializeObject(observableException.ExtraData)}");
                    this.txtLog.AppendText(Environment.NewLine);

                    this.txtLog.AppendText("++++++++++++++");
                    this.txtLog.AppendText(Environment.NewLine);
                }

                this.txtLog.AppendText("******************************************************************************");
                this.txtLog.AppendText(Environment.NewLine);
            }
            else
            {
                this.txtLog.Clear();
                this.txtLog.AppendText($"Message: {error.Message}");
                this.txtLog.AppendText(Environment.NewLine);

                this.txtLog.AppendText($"Exception: {error}");
                this.txtLog.AppendText(Environment.NewLine);

                this.txtLog.AppendText("******************************************************************************");
                this.txtLog.AppendText(Environment.NewLine);
            }
        }

        /// <inheritdoc />
        public void OnNext(PayloadMessage value)
        {
        }

        /// <summary>
        /// The btn clear_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnClearClick(object sender, EventArgs e) => this.txtLog.Clear();

        /// <summary>
        /// The btn reload_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnReloadClick(object sender, EventArgs e) => this.OnError(ExceptionManager.Current.Create());
    }
}