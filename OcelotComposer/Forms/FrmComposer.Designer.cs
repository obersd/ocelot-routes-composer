﻿namespace A2Net.OcelotComposer.Forms
{
    partial class FrmComposer
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnShowAll = new System.Windows.Forms.Button();
            this.lstComposeGroups = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lstComposeServer = new System.Windows.Forms.ComboBox();
            this.lstComposeGateway = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lstComposeEnv = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dgvComposeApis = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvComposeApis)).BeginInit();
            this.SuspendLayout();
            // 
            // btnShowAll
            // 
            this.btnShowAll.Location = new System.Drawing.Point(618, 38);
            this.btnShowAll.Name = "btnShowAll";
            this.btnShowAll.Size = new System.Drawing.Size(95, 25);
            this.btnShowAll.TabIndex = 9;
            this.btnShowAll.Text = "Mostrar todo";
            this.btnShowAll.UseVisualStyleBackColor = true;
            this.btnShowAll.Click += new System.EventHandler(this.BtnShowAllClick);
            // 
            // lstComposeGroups
            // 
            this.lstComposeGroups.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lstComposeGroups.FormattingEnabled = true;
            this.lstComposeGroups.Location = new System.Drawing.Point(378, 10);
            this.lstComposeGroups.Name = "lstComposeGroups";
            this.lstComposeGroups.Size = new System.Drawing.Size(216, 23);
            this.lstComposeGroups.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(329, 13);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 15);
            this.label8.TabIndex = 7;
            this.label8.Text = "Group:";
            // 
            // lstComposeServer
            // 
            this.lstComposeServer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lstComposeServer.FormattingEnabled = true;
            this.lstComposeServer.Location = new System.Drawing.Point(378, 38);
            this.lstComposeServer.Name = "lstComposeServer";
            this.lstComposeServer.Size = new System.Drawing.Size(216, 23);
            this.lstComposeServer.TabIndex = 5;
            // 
            // lstComposeGateway
            // 
            this.lstComposeGateway.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lstComposeGateway.FormattingEnabled = true;
            this.lstComposeGateway.Location = new System.Drawing.Point(97, 38);
            this.lstComposeGateway.Name = "lstComposeGateway";
            this.lstComposeGateway.Size = new System.Drawing.Size(217, 23);
            this.lstComposeGateway.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(36, 44);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 15);
            this.label7.TabIndex = 3;
            this.label7.Text = "Gateway:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(330, 41);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 15);
            this.label6.TabIndex = 2;
            this.label6.Text = "Server:";
            // 
            // lstComposeEnv
            // 
            this.lstComposeEnv.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lstComposeEnv.FormattingEnabled = true;
            this.lstComposeEnv.Location = new System.Drawing.Point(97, 9);
            this.lstComposeEnv.Name = "lstComposeEnv";
            this.lstComposeEnv.Size = new System.Drawing.Size(217, 23);
            this.lstComposeEnv.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 15);
            this.label5.TabIndex = 0;
            this.label5.Text = "Environment:";
            // 
            // dgvComposeApis
            // 
            this.dgvComposeApis.AllowUserToAddRows = false;
            this.dgvComposeApis.AllowUserToDeleteRows = false;
            this.dgvComposeApis.AllowUserToResizeColumns = false;
            this.dgvComposeApis.AllowUserToResizeRows = false;
            this.dgvComposeApis.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvComposeApis.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvComposeApis.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvComposeApis.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvComposeApis.Location = new System.Drawing.Point(13, 88);
            this.dgvComposeApis.Name = "dgvComposeApis";
            this.dgvComposeApis.ReadOnly = true;
            this.dgvComposeApis.RowHeadersWidth = 4;
            this.dgvComposeApis.Size = new System.Drawing.Size(709, 336);
            this.dgvComposeApis.TabIndex = 10;
            this.dgvComposeApis.Text = "dataGridView1";
            // 
            // FrmComposer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 484);
            this.Controls.Add(this.dgvComposeApis);
            this.Controls.Add(this.btnShowAll);
            this.Controls.Add(this.lstComposeGroups);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lstComposeServer);
            this.Controls.Add(this.lstComposeGateway);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lstComposeEnv);
            this.Controls.Add(this.label5);
            this.Name = "FrmComposer";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ocelot Routes Composer";
            ((System.ComponentModel.ISupportInitialize)(this.dgvComposeApis)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox lstComposeServer;
        private System.Windows.Forms.ComboBox lstComposeGateway;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox lstComposeEnv;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox lstComposeGroups;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnShowAll;
        private System.Windows.Forms.DataGridView dgvComposeApis;
    }
}

