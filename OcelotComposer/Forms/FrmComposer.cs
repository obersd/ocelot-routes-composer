﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FrmComposer.cs" company="A2net Studios">
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Alex Vargas.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace A2Net.OcelotComposer.Forms
{
    using System;
    using System.Linq;
    using System.Windows.Forms;

    using A2Net.OcelotComposer.Contexts;
    using A2Net.OcelotComposer.Models;

    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The frm main.
    /// </summary>
    public partial class FrmComposer : Form
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="FrmComposer"/> class from being created. 
        /// Initializes a new instance of the <see cref="FrmComposer"/> class.
        /// </summary>
        private FrmComposer()
        {
            this.InitializeComponent();
            this.Load += (sender, args) =>
                {
                    using (var ctx = OcelotContext.GetContext())
                    {
                        this.lstComposeEnv.DisplayMember = nameof(Environments.Name);
                        this.lstComposeEnv.ValueMember = nameof(Environments.Environmentid);
                        this.lstComposeEnv.DataSource = ctx.Environments.ToList();

                        this.lstComposeGroups.ValueMember = nameof(Groups.Groupid);
                        this.lstComposeGroups.DisplayMember = nameof(Groups.Name);
                        this.lstComposeGroups.DataSource = ctx.Groups.ToList();
                    }

                    this.lstComposeGroups.SelectedValueChanged += (sender, args) =>
                    {
                        var selectedGroup = this.lstComposeGroups.SelectedItem as Groups;
                        if (selectedGroup == null || selectedGroup.Groupid == -1)
                        {
                            this.dgvComposeApis.DataSource = null;
                            this.dgvComposeApis.Columns.Clear();
                            this.lstComposeServer.DataSource = null;
                            return;
                        }

                        var selectedEnvironment = this.lstComposeEnv.SelectedItem as Environments;
                        if (selectedEnvironment == null || selectedEnvironment.Environmentid == -1)
                        {
                            return;
                        }

                        using (var ctx = OcelotContext.GetContext())
                        {
                            this.lstComposeServer.ValueMember = nameof(Servers.Serverid);
                            this.lstComposeServer.DisplayMember = nameof(Servers.Appname);
                            var servers = ctx.Servers.Where(x => x.Isgateway == 0 && x.Groupid == selectedGroup.Groupid && x.Environmentid == selectedEnvironment.Environmentid)
                                .ToList();
                            this.lstComposeServer.DataSource = servers;
                        }
                    };
                    this.lstComposeEnv.SelectedValueChanged += (sender, args) =>
                    {
                        var selectedEnvironment = this.lstComposeEnv.SelectedItem as Environments;
                        if (selectedEnvironment == null || selectedEnvironment.Environmentid == -1)
                        {
                            this.dgvComposeApis.DataSource = null;
                            this.dgvComposeApis.Columns.Clear();
                            this.lstComposeServer.DataSource = null;
                            this.lstComposeGateway.DataSource = null;
                            return;
                        }

                        using (var ctx = OcelotContext.GetContext())
                        {
                            this.lstComposeGateway.ValueMember = nameof(Servers.Serverid);
                            this.lstComposeGateway.DisplayMember = nameof(Servers.Appname);
                            this.lstComposeGateway.DataSource = ctx.Servers.Where(x => x.Isgateway == 1 && x.Environmentid == selectedEnvironment.Environmentid).ToList();
                        }
                    };
                };
            
        }

        /// <summary>
        /// The show.
        /// </summary>
        /// <param name="window">
        /// The window.
        /// </param>
        /// <returns>
        /// The <see cref="Form"/>.
        /// </returns>
        public static Form Show(Form window)
        {
            var frm = new FrmComposer { MdiParent = window };
            frm.Show();
            return frm;
        }

        /// <summary>
        /// The btn show all_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnShowAllClick(object sender, EventArgs e)
        {
            var selectedGroupId = this.lstComposeGroups.SelectedValue is long value ? value : 0;
            var selectedGateway = this.lstComposeGateway.SelectedItem as Servers;
            var selectedServer = this.lstComposeServer.SelectedItem as Servers;

            if (selectedGateway == null || selectedServer == null)
            {
                return;
            }

            this.dgvComposeApis.AutoGenerateColumns = false;
            this.dgvComposeApis.DataSource = null;
            this.dgvComposeApis.Columns.Clear();
            this.dgvComposeApis.Columns.AddRange(
                new DataGridViewTextBoxColumn { Name = "Apiid", DataPropertyName = "Apiid", HeaderText = "Apiid", Visible = false },
                new DataGridViewTextBoxColumn { Name = "UriComplete", DataPropertyName = "UriComplete", HeaderText = "UriComplete" },
                new DataGridViewTextBoxColumn { Name = "GatewayComplete", DataPropertyName = "GatewayComplete", HeaderText = "GatewayComplete" },
                new DataGridViewTextBoxColumn { Name = "Controllerpath", DataPropertyName = "Controllerpath", HeaderText = "Controller Path" },
                new DataGridViewTextBoxColumn { Name = "Functionalitypath", DataPropertyName = "Functionalitypath", HeaderText = "Functionality Path" },
                new DataGridViewTextBoxColumn { Name = "Httpmethod", DataPropertyName = "Httpmethod", HeaderText = "Http Method" },
                new DataGridViewCheckBoxColumn { Name = "Issecure", DataPropertyName = "Issecure", HeaderText = "Is Secure" },
                new DataGridViewTextBoxColumn { Name = "Securekey", DataPropertyName = "Securekey", HeaderText = "Securekey" });

            using (var ctx = OcelotContext.GetContext())
            {
                var apis = ctx.Apis.Where(x => x.Groupid == selectedGroupId).Include(x => x.Group).Select(x => new Api(x, selectedServer, selectedGateway)).ToList();
                this.dgvComposeApis.DataSource = apis;
            }
        }
    }
}