﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WindowManager.cs" company="A2net Studios">
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Alex Vargas.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace A2Net.OcelotComposer.Forms
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Windows.Forms;

    /// <summary>
    /// The window manager.
    /// </summary>
    public sealed class WindowManager : IDisposable
    {
        /// <summary>
        /// The instance.
        /// </summary>
        private static readonly Lazy<WindowManager> Instance;

        /// <summary>
        /// Initializes static members of the <see cref="WindowManager"/> class.
        /// </summary>
        static WindowManager()
        {
            Instance = new Lazy<WindowManager>(() => new WindowManager(), LazyThreadSafetyMode.ExecutionAndPublication);
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="WindowManager"/> class from being created.
        /// </summary>
        private WindowManager()
        {
        }

        /// <summary>
        /// The window.
        /// </summary>
        public enum Window
        {
            /// <summary>
            /// The environments.
            /// </summary>
            Environments,

            /// <summary>
            /// The groups.
            /// </summary>
            Groups,

            /// <summary>
            /// The servers.
            /// </summary>
            Servers,

            /// <summary>
            /// The apis.
            /// </summary>
            Apis,

            /// <summary>
            /// The composer.
            /// </summary>
            Composer,

            /// <summary>
            /// The logger.
            /// </summary>
            Logger
        }

        /// <summary>
        /// The current.
        /// </summary>
        public static WindowManager Current => Instance.Value;

        /// <summary>
        /// Gets or sets the principal form.
        /// </summary>
        public Form PrincipalForm { get; set; }

        /// <inheritdoc />
        public void Dispose()
        {
            this.PrincipalForm?.Dispose();
            this.PrincipalForm = null;
        }

        /// <summary>
        /// The open.
        /// </summary>
        /// <param name="window">
        /// The window.
        /// </param>
        public void OpenChild(Window window)
        {
            var principal = this.PrincipalForm;
            var frm = this.GetMdiChildren(window, principal.MdiChildren);
            if (frm != null)
            {
                frm.WindowState = FormWindowState.Normal;
                frm.Activate();
                return;
            }

            switch (window)
            {
                case Window.Environments:
                    FrmEnvironments.Show(principal);
                    break;
                case Window.Groups:
                    FrmGroups.Show(principal);
                    break;
                case Window.Servers:
                    FrmServers.Show(principal);
                    break;
                case Window.Apis:
                    FrmApis.Show(principal);
                    break;
                case Window.Composer:
                    FrmComposer.Show(principal);
                    break;
                case Window.Logger:
                    FrmLogger.ShowWindow(principal);
                    break;
            }
        }

        /// <summary>
        /// The get mdi children.
        /// </summary>
        /// <param name="wnd">
        /// The wnd.
        /// </param>
        /// <param name="childs">
        /// The childs.
        /// </param>
        /// <returns>
        /// The <see cref="Form"/>.
        /// </returns>
        private Form GetMdiChildren(Window wnd, Form[] childs)
        {
            switch (wnd)
            {
                case Window.Environments:
                    return childs.FirstOrDefault(x => x is FrmEnvironments);
                case Window.Groups:
                    return childs.FirstOrDefault(x => x is FrmGroups);
                case Window.Servers:
                    return childs.FirstOrDefault(x => x is FrmServers);
                case Window.Apis:
                    return childs.FirstOrDefault(x => x is FrmApis);
                case Window.Composer:
                    return childs.FirstOrDefault(x => x is FrmComposer);
                case Window.Logger:
                    return childs.FirstOrDefault(x => x is FrmLogger);
                default:
                    return null;
            }
        }
    }
}