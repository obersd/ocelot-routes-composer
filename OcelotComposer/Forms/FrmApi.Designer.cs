﻿namespace A2Net.OcelotComposer.Forms
{
    partial class FrmApi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtController = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFunction = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lstMethod = new System.Windows.Forms.ComboBox();
            this.chkSecure = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSecureKey = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.lstGroup = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Controller path:";
            // 
            // txtController
            // 
            this.txtController.Location = new System.Drawing.Point(124, 25);
            this.txtController.Name = "txtController";
            this.txtController.Size = new System.Drawing.Size(276, 23);
            this.txtController.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Functionality path:";
            // 
            // txtFunction
            // 
            this.txtFunction.Location = new System.Drawing.Point(124, 54);
            this.txtFunction.Name = "txtFunction";
            this.txtFunction.Size = new System.Drawing.Size(276, 23);
            this.txtFunction.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(39, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "Http method:";
            // 
            // lstMethod
            // 
            this.lstMethod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lstMethod.FormattingEnabled = true;
            this.lstMethod.Location = new System.Drawing.Point(124, 83);
            this.lstMethod.Name = "lstMethod";
            this.lstMethod.Size = new System.Drawing.Size(276, 23);
            this.lstMethod.TabIndex = 3;
            // 
            // chkSecure
            // 
            this.chkSecure.AutoSize = true;
            this.chkSecure.Location = new System.Drawing.Point(47, 114);
            this.chkSecure.Name = "chkSecure";
            this.chkSecure.Size = new System.Drawing.Size(71, 19);
            this.chkSecure.TabIndex = 4;
            this.chkSecure.Tag = "";
            this.chkSecure.Text = "Is secure";
            this.chkSecure.UseVisualStyleBackColor = true;
            this.chkSecure.CheckedChanged += new System.EventHandler(this.ChkSecureCheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(124, 115);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 15);
            this.label4.TabIndex = 7;
            this.label4.Text = "Secure key:";
            // 
            // txtSecureKey
            // 
            this.txtSecureKey.Location = new System.Drawing.Point(196, 112);
            this.txtSecureKey.Name = "txtSecureKey";
            this.txtSecureKey.ReadOnly = true;
            this.txtSecureKey.Size = new System.Drawing.Size(204, 23);
            this.txtSecureKey.TabIndex = 5;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(324, 188);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 25);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "Guardar";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(243, 188);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 25);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Cancelar";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(75, 144);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 15);
            this.label5.TabIndex = 9;
            this.label5.Text = "Group:";
            // 
            // lstGroup
            // 
            this.lstGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lstGroup.FormattingEnabled = true;
            this.lstGroup.Location = new System.Drawing.Point(124, 141);
            this.lstGroup.Name = "lstGroup";
            this.lstGroup.Size = new System.Drawing.Size(276, 23);
            this.lstGroup.TabIndex = 6;
            // 
            // FrmApi
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(411, 225);
            this.Controls.Add(this.lstGroup);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtSecureKey);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.chkSecure);
            this.Controls.Add(this.lstMethod);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtFunction);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtController);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmApi";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "API";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtController;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtFunction;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox lstMethod;
        private System.Windows.Forms.CheckBox chkSecure;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtSecureKey;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox lstGroup;
    }
}