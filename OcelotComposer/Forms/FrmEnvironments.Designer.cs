﻿namespace A2Net.OcelotComposer.Forms
{
    partial class FrmEnvironments
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgEnvironments = new System.Windows.Forms.DataGridView();
            this.btnNew = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgEnvironments)).BeginInit();
            this.SuspendLayout();
            // 
            // dgEnvironments
            // 
            this.dgEnvironments.AllowUserToAddRows = false;
            this.dgEnvironments.AllowUserToDeleteRows = false;
            this.dgEnvironments.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgEnvironments.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgEnvironments.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgEnvironments.CausesValidation = false;
            this.dgEnvironments.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            this.dgEnvironments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgEnvironments.Location = new System.Drawing.Point(12, 12);
            this.dgEnvironments.Name = "dgEnvironments";
            this.dgEnvironments.ReadOnly = true;
            this.dgEnvironments.RowHeadersWidth = 23;
            this.dgEnvironments.Size = new System.Drawing.Size(420, 211);
            this.dgEnvironments.TabIndex = 0;
            this.dgEnvironments.Text = "dataGridView1";
            this.dgEnvironments.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgEnvironmentsCellContentClick);
            // 
            // btnNew
            // 
            this.btnNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNew.Location = new System.Drawing.Point(357, 244);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(75, 25);
            this.btnNew.TabIndex = 1;
            this.btnNew.Text = "&Nuevo...";
            this.btnNew.UseVisualStyleBackColor = true;
            // 
            // FrmEnvironments
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(444, 281);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.dgEnvironments);
            this.Name = "FrmEnvironments";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "Environments";
            ((System.ComponentModel.ISupportInitialize)(this.dgEnvironments)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgEnvironments;
        private System.Windows.Forms.Button btnNew;
    }
}