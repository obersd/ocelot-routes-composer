﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FrmGroups.cs" company="A2net Studios">
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Alex Vargas.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace A2Net.OcelotComposer.Forms
{
    using System.Linq;
    using System.Windows.Forms;

    using A2Net.OcelotComposer.Contexts;
    using A2Net.OcelotComposer.Models;
    using A2Net.OcelotComposer.Observer;

    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The frm groups.
    /// </summary>
    public partial class FrmGroups : Form
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="FrmGroups"/> class from being created. 
        /// Initializes a new instance of the <see cref="FrmGroups"/> class.
        /// </summary>
        private FrmGroups()
        {
            this.InitializeComponent();
            this.dgGroups.DataError += (sender, args) => MessageService.Current.NotifyException(args.Exception, "Error en DataGrid de Groups", new { Client = "Groups", Context = args });
            this.btnAdd.Click += (sender, args) =>
                {
                    var result = FrmGroupEnvironment.ShowGroup(this);
                    if (result)
                    {
                        this.LoadGroups();
                    }
                };
                
            this.LoadGroups();
        }

        /// <summary>
        /// The show.
        /// </summary>
        /// <param name="wnd">
        /// The wnd.
        /// </param>
        /// <returns>
        /// The <see cref="Form"/>.
        /// </returns>
        public static Form Show(Form wnd)
        {
            var frm = new FrmGroups { MdiParent = wnd };
            frm.Show();
            return frm;
        }

        /// <summary>
        /// The dg groups_ cell content click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void DgGroupsCellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                var id = (long)senderGrid["Groupid", e.RowIndex].Value;
                switch (e.ColumnIndex)
                {
                    case 1:
                        var result = FrmGroupEnvironment.ShowGroup(this, id);
                        if (result)
                        {
                            this.LoadGroups();
                        }

                        break;
                    case 2:
                        using (var ctx = OcelotContext.GetContext())
                        {
                            var entity = ctx.Groups.Include(x => x.Servers).Include(x => x.Apis).FirstOrDefault(x => x.Groupid == id);
                            if (entity != null && !entity.Servers.Any() && !entity.Apis.Any())
                            {
                                ctx.Groups.Remove(entity);
                                ctx.SaveChanges();
                            }
                        }

                        this.LoadGroups();
                        break;
                }
            }
        }
        
        /// <summary>
        /// The load groups.
        /// </summary>
        private void LoadGroups()
        {
            this.dgGroups.AutoGenerateColumns = false;
            this.dgGroups.DataSource = null;
            this.dgGroups.Columns.Clear();
            this.dgGroups.Columns.AddRange(
                new DataGridViewTextBoxColumn { HeaderText = "#", DataPropertyName = nameof(Group.Index) },
                new DataGridViewButtonColumn { Text = "Editar", UseColumnTextForButtonValue = true },
                new DataGridViewButtonColumn { Text = "Eliminar", UseColumnTextForButtonValue = true },
                new DataGridViewTextBoxColumn { Name = "Groupid", DataPropertyName = "Groupid", HeaderText = "Groupid", Visible = false },
                new DataGridViewTextBoxColumn { Name = "Name", DataPropertyName = "Name", HeaderText = "Name" });
            using (var ctx = OcelotContext.GetContext())
            {
                var index = 1;
                this.dgGroups.DataSource = ctx.Groups.AsEnumerable().Select(x => new Group(x, index++)).ToList();
            }
        }
    }
}