﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FrmApis.cs" company="A2net Studios">
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Alex Vargas.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace A2Net.OcelotComposer.Forms
{
    using System;
    using System.Linq;
    using System.Windows.Forms;

    using A2Net.OcelotComposer.Contexts;
    using A2Net.OcelotComposer.Models;
    using A2Net.OcelotComposer.Observer;

    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The frm apis.
    /// </summary>
    public partial class FrmApis : Form
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="FrmApis"/> class from being created.
        /// </summary>
        private FrmApis()
        {
            this.InitializeComponent();
            this.dgApis.DataError += (sender, args) => MessageService.Current.NotifyException(args.Exception, "Error en DataGrid de APIs", new { Client = "APIS", Context = args });
            this.dgApis.CellContentClick += this.DgApisCellContentClick;
            this.btnAdd.Click += this.BtnAddClick;
            this.Load += (sender, args) => this.LoadApis();
        }

        /// <summary>
        /// The show.
        /// </summary>
        /// <param name="wnd">
        /// The wnd.
        /// </param>
        /// <returns>
        /// The <see cref="Form"/>.
        /// </returns>
        public static Form Show(Form wnd)
        {
            var frm = new FrmApis { MdiParent = wnd };
            frm.Show();
            return frm;
        }

        /// <summary>
        /// The btn add click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnAddClick(object sender, EventArgs e)
        {
            var result = FrmApi.ShowWindow(this);
            if (result)
            {
                this.LoadApis();
                if (this.chkOpenNew.Checked)
                {
                    this.btnAdd.PerformClick();
                }
            }
        }

        /// <summary>
        /// The dg apis_ cell content click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void DgApisCellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            if (!(senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn) || e.RowIndex < 0)
            {
                return;
            }

            var id = (long)senderGrid["Apiid", e.RowIndex].Value;
            switch (e.ColumnIndex)
            {
                case 1:
                    var resultShow = FrmApi.ShowWindow(this, id);
                    if (!resultShow)
                    {
                        return;
                    }

                    MessageService.Current.NotifyItem(ItemActionMessage.Api, ItemActionMessage.ActionAdded);
                    this.LoadApis();
                    break;
                case 2:
                    var result = MessageBox.Show("¿Eliminar API?", "Ocelot Composer", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (result != DialogResult.Yes)
                    {
                        return;
                    }

                    using (var ctx = OcelotContext.GetContext())
                    {
                        var entity = ctx.Apis.FirstOrDefault(x => x.Apiid == id);
                        if (entity != null)
                        {
                            ctx.Apis.Remove(entity);
                            ctx.SaveChanges();
                            MessageService.Current.NotifyItem(ItemActionMessage.Api, ItemActionMessage.ActionDeleted);
                        }
                    }

                    this.LoadApis();
                    break;
            }
        }

        /// <summary>
        /// The load apis.
        /// </summary>
        private void LoadApis()
        {
            this.dgApis.AutoGenerateColumns = false;
            this.dgApis.DataSource = null;
            this.dgApis.Columns.Clear();
            this.dgApis.Columns.AddRange(
                new DataGridViewTextBoxColumn { HeaderText = "#", DataPropertyName = nameof(Api.Index) },
                new DataGridViewButtonColumn { Text = "Editar", UseColumnTextForButtonValue = true },
                new DataGridViewButtonColumn { Text = "Eliminar", UseColumnTextForButtonValue = true },
                new DataGridViewTextBoxColumn { Name = "Apiid", DataPropertyName = nameof(Api.Apiid), HeaderText = "Apiid", Visible = false },
                new DataGridViewTextBoxColumn { Name = "GroupName", DataPropertyName = nameof(Api.GroupName), HeaderText = "Group" },
                new DataGridViewTextBoxColumn { Name = "Controllerpath", DataPropertyName = nameof(Api.Controllerpath), HeaderText = "Controller Path" },
                new DataGridViewTextBoxColumn { Name = "Functionalitypath", DataPropertyName = nameof(Api.Functionalitypath), HeaderText = "Functionality Path" },
                new DataGridViewTextBoxColumn { Name = "Httpmethod", DataPropertyName = "Httpmethod", HeaderText = "Http Method" },
                new DataGridViewCheckBoxColumn { Name = "Issecure", DataPropertyName = "Issecure", HeaderText = "Is Secure?" },
                new DataGridViewTextBoxColumn { Name = "Securekey", DataPropertyName = "Securekey", HeaderText = "Secure Key" });

            using (var ctx = OcelotContext.GetContext())
            {
                int index = 1;
                this.dgApis.DataSource = ctx.Apis.Include(x => x.Group).AsEnumerable().Select(x => new Api(x, index++)).ToList();
            }
        }
    }
}