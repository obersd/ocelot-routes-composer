﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FrmServerInfo.cs" company="A2net Studios">
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Alex Vargas.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace A2Net.OcelotComposer.Forms
{
    using System;
    using System.Linq;
    using System.Windows.Forms;

    using A2Net.OcelotComposer.Contexts;
    using A2Net.OcelotComposer.Models;
    using A2Net.OcelotComposer.Observer;

    /// <summary>
    /// The frm server info.
    /// </summary>
    public partial class FrmServerInfo : Form
    {
        /// <summary>
        /// The id.
        /// </summary>
        private readonly long id;

        /// <summary>
        /// Initializes a new instance of the <see cref="FrmServerInfo"/> class.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        private FrmServerInfo(long id)
        {
            this.id = id;
            this.InitializeComponent();
            this.btnSave.Click += this.UpdateEntity;
            this.Load += (sender, args) =>
                {
                    this.Initialize();
                    using (var ctx = OcelotContext.GetContext())
                    {
                        var servers = ctx.Servers.FirstOrDefault(x => x.Serverid == id);
                        if (servers != null)
                        {
                            this.txtName.Text = servers.Appname;
                            this.lstSchemes.SelectedItem = servers.Scheme.ToUpperInvariant();
                            this.txtHost.Text = servers.Host;
                            this.nudPort.Value = servers.Port;
                            this.txtPart.Text = servers.Path;
                            this.lstEnvs.SelectedValue = servers.Environmentid;
                            this.txtSwagger.Text = servers.Swagger;
                            this.chkGateway.Checked = servers.Isgateway == 1;
                            this.lstGroup.SelectedValue = servers.Groupid;
                        }
                    }
                };
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="FrmServerInfo"/> class from being created.
        /// </summary>
        private FrmServerInfo()
        {
            this.InitializeComponent();
            this.Load += (sender, args) => this.Initialize();
            this.btnSave.Click += this.AddEntity;
        }

        /// <summary>
        /// The show.
        /// </summary>
        /// <param name="wnd">
        /// The wnd.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool ShowWindow(IWin32Window wnd)
        {
            var frm = new FrmServerInfo();
            var result = frm.ShowDialog(wnd);
            return result == DialogResult.OK;
        }

        /// <summary>
        /// The show.
        /// </summary>
        /// <param name="wnd">
        /// The wnd.
        /// </param>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool ShowWindow(IWin32Window wnd, long id)
        {
            var frm = new FrmServerInfo(id);
            var result = frm.ShowDialog(wnd);
            return result == DialogResult.OK;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void AddEntity(object sender, EventArgs e)
        {
            var server = new Servers
            {
                Appname = this.txtName.Text,
                Environmentid = ((Environments)this.lstEnvs.SelectedItem).Environmentid,
                Groupid = (long)this.lstGroup.SelectedValue,
                Isgateway = this.chkGateway.Checked ? 1 : 0,
                Path = this.GetPath(),
                Port = (long)this.nudPort.Value,
                Scheme = this.lstSchemes.SelectedValue.ToString(),
                Host = this.txtHost.Text,
                Swagger = this.txtSwagger.Text,
            };

            using (var ctx = OcelotContext.GetContext())
            {
                ctx.Servers.Add(server);
                ctx.SaveChanges();
            }

            this.DialogResult = DialogResult.OK;
        }

        /// <summary>
        /// The btn view click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnViewClick(object sender, EventArgs e)
        {
            var server = new Servers
            {
                Host = this.txtHost.Text,
                Port = (long)this.nudPort.Value,
                Path = this.GetPath(),
                Scheme = this.lstSchemes.SelectedValue?.ToString()?.ToLowerInvariant(),
                Swagger = this.txtSwagger.Text
            };

            var env = new Server(server);
            this.txtComplete.Clear();
            try
            {
                this.txtComplete.AppendText(env.Complete.AbsoluteUri);
                this.txtComplete.AppendText(System.Environment.NewLine);
                this.txtComplete.AppendText(env.SwaggerComplete.AbsoluteUri);
            }
            catch (Exception ex)
            {
                MessageService.Current.NotifyException(ex, "Error al mostrar rutas de Servidor", new { server });
            }
        }

        /// <summary>
        /// The chk gateway_ checked changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void ChkGatewayCheckedChanged(object sender, EventArgs e)
        {
            if (this.chkGateway.Checked)
            {
                this.txtSwagger.Clear();
                this.txtSwagger.ReadOnly = true;
            }
            else
            {
                this.txtSwagger.ReadOnly = false;
            }
        }

        /// <summary>
        /// The get path.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetPath()
        {
            string path;
            if (this.txtPart.Text.Length == 0)
            {
                path = "/";
            }
            else
            {
                path = !this.txtPart.Text.StartsWith("/") ? $"/{this.txtPart.Text}" : this.txtPart.Text;
                if (!this.txtPart.Text.EndsWith("/"))
                {
                    path = $"{path}/";
                }
            }

            return path;
        }

        /// <summary>
        /// The initialize.
        /// </summary>
        private void Initialize()
        {
            using (var ctx = OcelotContext.GetContext())
            {
                this.lstEnvs.DisplayMember = nameof(Environments.Name);
                this.lstEnvs.ValueMember = nameof(Environments.Environmentid);
                this.lstEnvs.DataSource = ctx.Environments.OrderBy(x => x.Name).ToList();

                this.lstGroup.DisplayMember = nameof(Groups.Name);
                this.lstGroup.ValueMember = nameof(Groups.Groupid);
                this.lstGroup.DataSource = ctx.Groups.OrderBy(x => x.Name).ToList();
            }

            var schemes = new[] { "HTTPS", "HTTP" };
            this.lstSchemes.DataSource = schemes;
            this.btnCancel.DialogResult = DialogResult.Cancel;

            var message = string.Empty;
            if (this.lstEnvs.Items.Count == 0)
            {
                message = " entornos ";
            }

            if (this.lstGroup.Items.Count == 0)
            {
                message = message.Length == 0 ? " grupos " : $"{message}ni grupos ";
            }

            if (message.Length != 0)
            {
                MessageBox.Show($"No existen{message}para agregar un servidor.", "Ocelot Composer", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.DialogResult = DialogResult.Cancel;
            }
        }

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void UpdateEntity(object sender, EventArgs e)
        {
            using (var ctx = OcelotContext.GetContext())
            {
                var servers = ctx.Servers.FirstOrDefault(x => x.Serverid == this.id);
                if (servers != null)
                {
                    servers.Environmentid = ((Environments)this.lstEnvs.SelectedItem).Environmentid;
                    servers.Groupid = (long)this.lstGroup.SelectedValue;
                    servers.Appname = this.txtName.Text;
                    servers.Isgateway = this.chkGateway.Checked ? 1 : 0;
                    servers.Path = this.GetPath();
                    servers.Port = (long)this.nudPort.Value;
                    servers.Scheme = this.lstSchemes.SelectedValue.ToString();
                    servers.Host = this.txtHost.Text;
                    servers.Swagger = this.txtSwagger.Text;
                    ctx.SaveChanges();
                }
            }

            this.DialogResult = DialogResult.OK;
        }

        private void nudPort_Enter(object sender, EventArgs e)
        {
            NumericUpDown nud = sender as NumericUpDown;
            nud.Select(0, nud.Text.Length);
        }
    }
}