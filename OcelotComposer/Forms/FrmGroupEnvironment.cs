﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FrmGroupEnvironment.cs" company="A2net Studios">
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Alex Vargas.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace A2Net.OcelotComposer.Forms
{
    using System.Linq;
    using System.Windows.Forms;

    using A2Net.OcelotComposer.Contexts;

    /// <summary>
    /// The frm group environment.
    /// </summary>
    public partial class FrmGroupEnvironment : Form
    {
        /// <summary>
        /// The type env.
        /// </summary>
        private const string TypeEnv = "env";

        /// <summary>
        /// The type group.
        /// </summary>
        private const string TypeGroup = "grp";

        /// <summary>
        /// The id.
        /// </summary>
        private readonly long id;

        /// <summary>
        /// The type.
        /// </summary>
        private readonly string type;

        /// <summary>
        /// Initializes a new instance of the <see cref="FrmGroupEnvironment"/> class.
        /// </summary>
        public FrmGroupEnvironment()
        {
            this.InitializeComponent();
            this.btnCancel.DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FrmGroupEnvironment"/> class.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        private FrmGroupEnvironment(long id, string type)
            : this()
        {
            this.id = id;
            this.type = type;
            if (this.type == TypeEnv)
            {
                this.InitializeEnvironment();
            }
            else
            {
                this.InitializeGroup();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FrmGroupEnvironment"/> class.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        private FrmGroupEnvironment(string type)
            : this()
        {
            this.type = type;
            if (this.type == TypeEnv)
            {
                this.InitializeEnvironmentNew();
            }
            else
            {
                this.InitializeGroupNew();
            }
        }

        /// <summary>
        /// The show environment.
        /// </summary>
        /// <param name="wnd">
        /// The wnd.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool ShowEnvironment(IWin32Window wnd)
        {
            var frm = new FrmGroupEnvironment(TypeEnv);
            var result = frm.ShowDialog(wnd);
            return result == DialogResult.OK;
        }

        /// <summary>
        /// The show environment.
        /// </summary>
        /// <param name="wnd">
        /// The wnd.
        /// </param>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool ShowEnvironment(IWin32Window wnd, long id)
        {
            var frm = new FrmGroupEnvironment(id, TypeEnv);
            var result = frm.ShowDialog(wnd);
            return result == DialogResult.OK;
        }

        /// <summary>
        /// The show group.
        /// </summary>
        /// <param name="wnd">
        /// The wnd.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool ShowGroup(IWin32Window wnd)
        {
            var frm = new FrmGroupEnvironment(TypeGroup);
            var result = frm.ShowDialog(wnd);
            return result == DialogResult.OK;
        }

        /// <summary>
        /// The show group.
        /// </summary>
        /// <param name="wnd">
        /// The wnd.
        /// </param>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool ShowGroup(IWin32Window wnd, long id)
        {
            var frm = new FrmGroupEnvironment(id, TypeGroup);
            var result = frm.ShowDialog(wnd);
            return result == DialogResult.OK;
        }

        /// <summary>
        /// The initialize environment.
        /// </summary>
        private void InitializeEnvironment()
        {
            this.Text = "Environment";
            using (var ctx = OcelotContext.GetContext())
            {
                var env = ctx.Environments.FirstOrDefault(x => x.Environmentid == this.id);
                if (env == null)
                {
                    this.DialogResult = DialogResult.Cancel;
                }
                else
                {
                    this.txtName.Text = env.Name;
                }
            }

            this.btnSave.Click += (sender, args) =>
                {
                    if (string.IsNullOrWhiteSpace(this.txtName.Text))
                    {
                        MessageBox.Show("Introducir nombre.", "Ocelot Composer", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    bool save = false;
                    using (var ctx = OcelotContext.GetContext())
                    {
                        var grp = ctx.Environments.FirstOrDefault(x => x.Name == this.txtName.Text && x.Environmentid != this.id);
                        if (grp == null)
                        {
                            save = true;
                            var newGrp = ctx.Environments.FirstOrDefault(x => x.Environmentid == this.id);
                            if (newGrp == null)
                            {
                                newGrp = new Environments { Name = this.txtName.Text };
                                ctx.Environments.Add(newGrp);
                            }

                            newGrp.Name = this.txtName.Text;
                            ctx.SaveChanges();
                        }
                    }

                    if (!save)
                    {
                        MessageBox.Show("El entorno ya existe.", "Ocelot Composer", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else
                    {
                        this.DialogResult = DialogResult.OK;
                    }
                };
        }

        /// <summary>
        /// The initialize environment new.
        /// </summary>
        private void InitializeEnvironmentNew()
        {
            this.Text = "Environment";
            this.btnSave.Click += (sender, args) =>
                {
                    if (string.IsNullOrWhiteSpace(this.txtName.Text))
                    {
                        MessageBox.Show("Introducir nombre.", "Ocelot Composer", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    bool save = false;
                    using (var ctx = OcelotContext.GetContext())
                    {
                        var grp = ctx.Environments.FirstOrDefault(x => x.Name == this.txtName.Text);
                        if (grp == null)
                        {
                            save = true;
                            grp = new Environments { Name = this.txtName.Text };
                            ctx.Environments.Add(grp);
                            ctx.SaveChanges();
                        }
                    }

                    if (!save)
                    {
                        MessageBox.Show("El entorno ya existe.", "Ocelot Composer", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else
                    {
                        this.DialogResult = DialogResult.OK;
                    }
                };
        }

        /// <summary>
        /// The initialize group.
        /// </summary>
        private void InitializeGroup()
        {
            this.Text = "Group";
            using (var ctx = OcelotContext.GetContext())
            {
                var grp = ctx.Groups.FirstOrDefault(x => x.Groupid == this.id);
                if (grp == null)
                {
                    this.DialogResult = DialogResult.Cancel;
                }
                else
                {
                    this.txtName.Text = grp.Name;
                }
            }

            this.btnSave.Click += (sender, args) =>
                {
                    if (string.IsNullOrWhiteSpace(this.txtName.Text))
                    {
                        MessageBox.Show("Introducir nombre.", "Ocelot Composer", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    bool save = false;
                    using (var ctx = OcelotContext.GetContext())
                    {
                        var grp = ctx.Groups.FirstOrDefault(x => x.Name == this.txtName.Text && x.Groupid != this.id);
                        if (grp == null)
                        {
                            save = true;
                            var newGrp = ctx.Groups.FirstOrDefault(x => x.Groupid == this.id);
                            if (newGrp == null)
                            {
                                newGrp = new Groups { Name = this.txtName.Text };
                                ctx.Groups.Add(newGrp);
                            }

                            newGrp.Name = this.txtName.Text;
                            ctx.SaveChanges();
                        }
                    }

                    if (!save)
                    {
                        MessageBox.Show("El grupo ya existe.", "Ocelot Composer", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else
                    {
                        this.DialogResult = DialogResult.OK;
                    }
                };
        }

        /// <summary>
        /// The initialize group new.
        /// </summary>
        private void InitializeGroupNew()
        {
            this.Text = "Group";
            this.btnSave.Click += (sender, args) =>
                {
                    if (string.IsNullOrWhiteSpace(this.txtName.Text))
                    {
                        MessageBox.Show("Introducir nombre.", "Ocelot Composer", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    bool save = false;
                    using (var ctx = OcelotContext.GetContext())
                    {
                        var grp = ctx.Groups.FirstOrDefault(x => x.Name == this.txtName.Text);
                        if (grp == null)
                        {
                            save = true;
                            grp = new Groups { Name = this.txtName.Text };
                            ctx.Groups.Add(grp);
                            ctx.SaveChanges();
                        }
                    }

                    if (!save)
                    {
                        MessageBox.Show("El grupo ya existe.", "Ocelot Composer", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else
                    {
                        this.DialogResult = DialogResult.OK;
                    }
                };
        }
    }
}