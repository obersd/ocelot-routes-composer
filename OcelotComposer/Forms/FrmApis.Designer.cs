﻿namespace A2Net.OcelotComposer.Forms
{
    partial class FrmApis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAdd = new System.Windows.Forms.Button();
            this.dgApis = new System.Windows.Forms.DataGridView();
            this.chkOpenNew = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgApis)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Location = new System.Drawing.Point(357, 244);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 25);
            this.btnAdd.TabIndex = 1;
            this.btnAdd.Text = "&Nuevo...";
            this.btnAdd.UseVisualStyleBackColor = true;
            // 
            // dgApis
            // 
            this.dgApis.AllowUserToAddRows = false;
            this.dgApis.AllowUserToDeleteRows = false;
            this.dgApis.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgApis.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgApis.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgApis.CausesValidation = false;
            this.dgApis.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            this.dgApis.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgApis.Location = new System.Drawing.Point(12, 12);
            this.dgApis.Name = "dgApis";
            this.dgApis.ReadOnly = true;
            this.dgApis.RowHeadersWidth = 23;
            this.dgApis.Size = new System.Drawing.Size(420, 211);
            this.dgApis.TabIndex = 0;
            this.dgApis.Text = "dataGridView1";
            // 
            // chkOpenNew
            // 
            this.chkOpenNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkOpenNew.AutoSize = true;
            this.chkOpenNew.Location = new System.Drawing.Point(12, 244);
            this.chkOpenNew.Name = "chkOpenNew";
            this.chkOpenNew.Size = new System.Drawing.Size(229, 19);
            this.chkOpenNew.TabIndex = 2;
            this.chkOpenNew.Text = "Agregar nuevo y volver a abrir ventana";
            this.chkOpenNew.UseVisualStyleBackColor = true;
            // 
            // FrmApis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(444, 281);
            this.Controls.Add(this.chkOpenNew);
            this.Controls.Add(this.dgApis);
            this.Controls.Add(this.btnAdd);
            this.Name = "FrmApis";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "APIs";
            ((System.ComponentModel.ISupportInitialize)(this.dgApis)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.DataGridView dgApis;
        private System.Windows.Forms.CheckBox chkOpenNew;
    }
}