﻿namespace A2Net.OcelotComposer.Forms
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mnsMain = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.mniQuit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.mniEnvironments = new System.Windows.Forms.ToolStripMenuItem();
            this.mniGroups = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mniServers = new System.Windows.Forms.ToolStripMenuItem();
            this.mniApis = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.mniArrange = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.mniCascade = new System.Windows.Forms.ToolStripMenuItem();
            this.mniH = new System.Windows.Forms.ToolStripMenuItem();
            this.mniV = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.mniGenerator = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.mniViewer = new System.Windows.Forms.ToolStripMenuItem();
            this.mnsMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // mnsMain
            // 
            this.mnsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4});
            this.mnsMain.Location = new System.Drawing.Point(0, 0);
            this.mnsMain.MdiWindowListItem = this.toolStripMenuItem3;
            this.mnsMain.Name = "mnsMain";
            this.mnsMain.Size = new System.Drawing.Size(800, 24);
            this.mnsMain.TabIndex = 3;
            this.mnsMain.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mniQuit});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(60, 20);
            this.toolStripMenuItem1.Text = "Archivo";
            // 
            // mniQuit
            // 
            this.mniQuit.Name = "mniQuit";
            this.mniQuit.Size = new System.Drawing.Size(96, 22);
            this.mniQuit.Text = "Salir";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mniEnvironments,
            this.mniGroups,
            this.toolStripSeparator1,
            this.mniServers,
            this.mniApis});
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(72, 20);
            this.toolStripMenuItem2.Text = "Catálogos";
            // 
            // mniEnvironments
            // 
            this.mniEnvironments.Name = "mniEnvironments";
            this.mniEnvironments.Size = new System.Drawing.Size(128, 22);
            this.mniEnvironments.Text = "Entornos";
            // 
            // mniGroups
            // 
            this.mniGroups.Name = "mniGroups";
            this.mniGroups.Size = new System.Drawing.Size(128, 22);
            this.mniGroups.Text = "Grupos";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(125, 6);
            // 
            // mniServers
            // 
            this.mniServers.Name = "mniServers";
            this.mniServers.Size = new System.Drawing.Size(128, 22);
            this.mniServers.Text = "Servidores";
            // 
            // mniApis
            // 
            this.mniApis.Name = "mniApis";
            this.mniApis.Size = new System.Drawing.Size(128, 22);
            this.mniApis.Text = "APIs";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mniArrange,
            this.toolStripSeparator4,
            this.mniCascade,
            this.mniH,
            this.mniV});
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(35, 20);
            this.toolStripMenuItem3.Text = "Ver";
            // 
            // mniArrange
            // 
            this.mniArrange.Name = "mniArrange";
            this.mniArrange.Size = new System.Drawing.Size(163, 22);
            this.mniArrange.Text = "Org‭anizar íconos";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(160, 6);
            // 
            // mniCascade
            // 
            this.mniCascade.Name = "mniCascade";
            this.mniCascade.Size = new System.Drawing.Size(163, 22);
            this.mniCascade.Text = "Cascada";
            // 
            // mniH
            // 
            this.mniH.Name = "mniH";
            this.mniH.Size = new System.Drawing.Size(163, 22);
            this.mniH.Text = "Horizontal";
            // 
            // mniV
            // 
            this.mniV.Name = "mniV";
            this.mniV.Size = new System.Drawing.Size(163, 22);
            this.mniV.Text = "Vertical";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mniGenerator,
            this.toolStripSeparator3,
            this.mniViewer});
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(90, 20);
            this.toolStripMenuItem4.Text = "Herramientas";
            // 
            // mniGenerator
            // 
            this.mniGenerator.Name = "mniGenerator";
            this.mniGenerator.Size = new System.Drawing.Size(160, 22);
            this.mniGenerator.Text = "Generador";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(157, 6);
            // 
            // mniViewer
            // 
            this.mniViewer.Name = "mniViewer";
            this.mniViewer.Size = new System.Drawing.Size(160, 22);
            this.mniViewer.Text = "Visor de eventos";
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.mnsMain);
            this.IsMdiContainer = true;
            this.Name = "FrmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ocelot Routes Composer";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.mnsMain.ResumeLayout(false);
            this.mnsMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mnsMain;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem mniEnvironments;
        private System.Windows.Forms.ToolStripMenuItem mniGroups;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem mniServers;
        private System.Windows.Forms.ToolStripMenuItem mniApis;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem mniCascade;
        private System.Windows.Forms.ToolStripMenuItem mniH;
        private System.Windows.Forms.ToolStripMenuItem mniV;
        private System.Windows.Forms.ToolStripMenuItem mniArrange;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem mniQuit;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem mniGenerator;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem mniViewer;
    }
}