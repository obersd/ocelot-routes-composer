﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MessageService.cs" company="A2net Studios">
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Alex Vargas.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace A2Net.OcelotComposer.Observer
{
    using System;
    using System.Collections.Generic;
    using System.Threading;

    using A2Net.OcelotComposer.Helpers;

    /// <summary>
    /// The message service.
    /// </summary>
    public sealed class MessageService : IObservable<PayloadMessage>
    {
        /// <summary>
        /// The instance.
        /// </summary>
        private static readonly Lazy<MessageService> Instance;

        /// <summary>
        /// The observers.
        /// </summary>
        private IList<IObserver<PayloadMessage>> observers;

        /// <summary>
        /// Initializes static members of the <see cref="MessageService"/> class.
        /// </summary>
        static MessageService()
        {
            Instance = new Lazy<MessageService>(() => new MessageService(), LazyThreadSafetyMode.ExecutionAndPublication);
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="MessageService"/> class from being created.
        /// </summary>
        private MessageService()
        {
            this.observers = new List<IObserver<PayloadMessage>>();
        }

        /// <summary>
        /// The current.
        /// </summary>
        public static MessageService Current => Instance.Value;

        /// <summary>
        /// The notify exception.
        /// </summary>
        /// <param name="exception">
        /// The exception.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="extra">
        /// The extra.
        /// </param>
        public void NotifyException(Exception exception, string message, dynamic extra)
        {
            ExceptionManager.Current.Exceptions.Add(new ObservableException(message, exception, extra));
            foreach (var observer in this.observers)
            {
                observer.OnError(ExceptionManager.Current.Create());
            }
        }

        /// <summary>
        /// The notify new api.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <param name="action">
        /// The action.
        /// </param>
        public void NotifyItem(int item, int action)
        {
            foreach (var observer in this.observers)
            {
                observer.OnNext(new ItemActionMessage(item, action));
            }
        }

        /// <inheritdoc />
        public IDisposable Subscribe(IObserver<PayloadMessage> observer)
        {
            if (this.observers == null)
            {
                this.observers = new List<IObserver<PayloadMessage>>();
            }

            if (!this.observers.Contains(observer))
            {
                this.observers.Add(observer);
            }

            return new Unsubscriber(this.observers, observer);
        }
    }
}