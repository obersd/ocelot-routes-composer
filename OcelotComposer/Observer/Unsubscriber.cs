﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Unsubscriber.cs" company="A2net Studios">
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Alex Vargas.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace A2Net.OcelotComposer.Observer
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The unsubscriber.
    /// </summary>
    public class Unsubscriber : IDisposable
    {
        /// <summary>
        /// The observer.
        /// </summary>
        private readonly IObserver<PayloadMessage> observer;

        /// <summary>
        /// The observers.
        /// </summary>
        private readonly IList<IObserver<PayloadMessage>> observers;

        /// <summary>
        /// Initializes a new instance of the <see cref="Unsubscriber"/> class.
        /// </summary>
        /// <param name="observers">
        /// The observers.
        /// </param>
        /// <param name="observer">
        /// The observer.
        /// </param>
        public Unsubscriber(IList<IObserver<PayloadMessage>> observers, IObserver<PayloadMessage> observer)
        {
            this.observers = observers;
            this.observer = observer;
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        public void Dispose()
        {
            if (this.observer != null && this.observers != null && this.observers.Contains(this.observer))
            {
                this.observers.Remove(this.observer);
            }
        }
    }
}