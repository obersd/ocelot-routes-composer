﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ObservableException.cs" company="A2net Studios">
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Alex Vargas.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace A2Net.OcelotComposer.Observer
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The observable exception.
    /// </summary>
    public class ObservableException : Exception
    {
        /// <inheritdoc />
        public ObservableException(string? message, Exception? innerException, dynamic extraData)
            : base(message, innerException)
        {
            this.ExtraData = extraData;
        }

        /// <summary>
        /// Gets the extra data.
        /// </summary>
        public dynamic ExtraData { get; }
    }

    /// <summary>
    /// The observable exceptions.
    /// </summary>
    public class ObservableExceptions : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ObservableExceptions"/> class.
        /// </summary>
        /// <param name="exceptions">
        /// The exceptions.
        /// </param>
        public ObservableExceptions(IEnumerable<Exception> exceptions)
        {
            this.Exceptions = exceptions;
        }

        /// <summary>
        /// Gets the exceptions.
        /// </summary>
        public IEnumerable<Exception> Exceptions { get; }
    }
}