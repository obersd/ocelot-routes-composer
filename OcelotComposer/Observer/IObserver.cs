﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IObserver.cs" company="A2net Studios">
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Alex Vargas.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace A2Net.OcelotComposer.Observer
{
    using System;

    /// <summary>
    /// The ObserverData interface.
    /// </summary>
    public interface IObserver : IObserver<PayloadMessage>
    {
        /// <summary>
        /// Gets or sets the unsubscriber.
        /// </summary>
        IDisposable Unsubscriber { get; set; }
    }
}