﻿namespace A2Net.OcelotComposer.Observer
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The PayloadMessage interface.
    /// </summary>
    public abstract class PayloadMessage
    {
    }

    public class ExceptionOcurredMessage : PayloadMessage
    {
    }
    public class ItemActionMessage : PayloadMessage
    {
        public const int Environment = 1;
        public const int Group = 2;
        public const int Server = 3;
        public const int Api = 4;
        public const int ActionDeleted = 0;
        public const int ActionAdded = 1;

        public const int ActionUpdated = 2;


        public int Item { get; set; }

        public int Action { get; set; }

        public ItemActionMessage(int item, int action)
        {
            this.Item = item;
            this.Action = action;
        }
    }
}