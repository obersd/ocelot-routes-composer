﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Environment.cs" company="A2net Studios">
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Alex Vargas.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace A2Net.OcelotComposer.Models
{
    using A2Net.OcelotComposer.Contexts;

    /// <summary>
    /// The environment.
    /// </summary>
    public class Environment : Environments
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Environment"/> class.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <param name="index">
        /// The index.
        /// </param>
        public Environment(Environments item, int index)
        {
            this.Index = index;
            this.Environmentid = item.Environmentid;
            this.Name = item.Name;
            this.Servers = item.Servers;
        }

        /// <summary>
        /// Gets or sets the index.
        /// </summary>
        public int Index { get; set; }
    }
}