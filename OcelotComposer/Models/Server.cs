﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Server.cs" company="A2net Studios">
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Alex Vargas.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace A2Net.OcelotComposer.Models
{
    using System;

    using A2Net.OcelotComposer.Contexts;
    using A2Net.OcelotComposer.Helpers;

    /// <summary>
    /// The server.
    /// </summary>
    public class Server : Servers
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Server"/> class.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public Server(Servers item)
        {
            this.Environment = item.Environment;
            this.Host = item.Host;
            this.Appname = item.Appname;
            this.Environmentid = item.Environmentid;
            this.Isgateway = item.Isgateway;
            this.Path = item.Path;
            this.Port = item.Port;
            this.Scheme = item.Scheme;
            this.Serverid = item.Serverid;
            this.Swagger = item.Swagger;
            this.Group = item.Group;
            this.Groupid = item.Groupid;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Server"/> class.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <param name="index">
        /// The index.
        /// </param>
        public Server(Servers item, int index)
            : this(item)
        {
            this.Index = index;
        }

        /// <summary>
        /// The complete.
        /// </summary>
        public Uri Complete => StringPaths.Build(this.Scheme, this.Host, this.Port, this.Path);

        /// <summary>
        /// The environment name.
        /// </summary>
        public string EnvironmentName => this.Environment?.Name ?? string.Empty;

        /// <summary>
        /// The group name.
        /// </summary>
        public string GroupName => this.Group?.Name ?? string.Empty;

        /// <summary>
        /// Gets or sets the index.
        /// </summary>
        public int Index { get; set; }

        /// <summary>
        /// The swagger complete.
        /// </summary>
        public Uri SwaggerComplete => this.Complete.Append(this.Swagger);
    }
}