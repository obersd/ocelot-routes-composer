﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Api.cs" company="A2net Studios">
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Alex Vargas.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace A2Net.OcelotComposer.Models
{
    using A2Net.OcelotComposer.Contexts;
    using A2Net.OcelotComposer.Helpers;

    /// <summary>
    /// The api.
    /// </summary>
    public class Api : Apis
    {
        /// <summary>
        /// The gateway.
        /// </summary>
        private readonly Server gateway;

        /// <summary>
        /// The server.
        /// </summary>
        private readonly Server server;

        /// <summary>
        /// Initializes a new instance of the <see cref="Api"/> class.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        public Api(Apis item)
        {
            this.Issecure = item.Issecure;
            this.Controllerpath = item.Controllerpath;
            this.Functionalitypath = item.Functionalitypath;
            this.Securekey = item.Securekey;
            this.Httpmethod = item.Httpmethod;
            this.Apiid = item.Apiid;
            this.Groupid = item.Groupid;
            this.Group = item.Group;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Api"/> class.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <param name="index">
        /// The index.
        /// </param>
        public Api(Apis item, int index)
            : this(item)
        {
            this.Index = index;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Api"/> class.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <param name="server">
        /// The server.
        /// </param>
        /// <param name="gateway">
        /// The gateway.
        /// </param>
        public Api(Apis item, Servers server, Servers gateway)
            : this(item)
        {
            this.server = new Server(server);
            this.gateway = new Server(gateway);
            this.Configure();
        }

        /// <summary>
        /// Gets or sets the gateway complete.
        /// </summary>
        public string GatewayComplete { get; set; }

        /// <summary>
        /// The group name.
        /// </summary>
        public string GroupName => this.Group?.Name ?? string.Empty;

        /// <summary>
        /// Gets or sets the index.
        /// </summary>
        public int Index { get; set; }

        /// <summary>
        /// Gets or sets the uri complete.
        /// </summary>
        public string UriComplete { get; set; }

        /// <summary>
        /// The configure.
        /// </summary>
        private void Configure()
        {
            this.UriComplete = this.server.Complete.Append(this.Controllerpath, this.Functionalitypath).AbsoluteUri;
            if (this.UriComplete.EndsWith("/"))
            {
                this.UriComplete = this.UriComplete.TrimEnd('/');
            }

            this.UriComplete = this.UriComplete.Replace("%7D", "}").Replace("%7B", "{");

            this.GatewayComplete = this.gateway.Complete.Append(this.Controllerpath, this.Functionalitypath).AbsoluteUri.Replace("%7D", "}").Replace("%7B", "{");
            if (this.GatewayComplete.EndsWith("/"))
            {
                this.GatewayComplete = this.GatewayComplete.TrimEnd('/');
            }
        }
    }
}