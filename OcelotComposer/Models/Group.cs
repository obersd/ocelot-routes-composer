﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Group.cs" company="A2net Studios">
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de Alex Vargas.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace A2Net.OcelotComposer.Models
{
    using A2Net.OcelotComposer.Contexts;

    /// <summary>
    /// The group.
    /// </summary>
    public class Group : Groups
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Group"/> class.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <param name="index">
        /// The index.
        /// </param>
        public Group(Groups item, int index)
        {
            this.Index = index;
            this.Apis = item.Apis;
            this.Groupid = item.Groupid;
            this.Name = item.Name;
            this.Servers = item.Servers;
        }

        /// <summary>
        /// Gets or sets the index.
        /// </summary>
        public int Index { get; set; }
    }
}